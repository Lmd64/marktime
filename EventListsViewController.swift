//
//  EventListsViewController.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit
import CoreData

class EventListCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleTextView: UITextView!
    @IBOutlet weak var eventCountLabel: UILabel!
    @IBOutlet weak var lastEditedLabel: UILabel!
}

class EventListsViewController: UITableViewController, NSFetchedResultsControllerDelegate, DismissControllerDelegate {

    let dataModel = DataModel.sharedInstance
    let dateFormatter: NSDateFormatter = NSDateFormatter()

    var eventListViewController: EventListViewController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            //self.eventListViewController = (controllers.last as! UINavigationController).topViewController as? EventListViewController
            self.eventListViewController = (controllers.first as! UINavigationController).topViewController as? EventListViewController
        }
    
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        
    }

    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let splitViewController = appDelegate.window!.rootViewController as! UISplitViewController
        if splitViewController.viewControllers.count > 1 {
            let controller = EventListsViewController.blankDetailViewController()
            splitViewController.showDetailViewController(controller, sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showEvent" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                if let eventList :EventList = self.fetchedResultsController.objectAtIndexPath(indexPath) as? EventList {
                    //let controller = (segue.destinationViewController as! UINavigationController).topViewController as! EventListViewController
                    let controller = segue.destinationViewController as! EventListViewController
                    controller.eventList = eventList
                }
            }
        }
    }

    // MARK: - Table View

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("EventListCell", forIndexPath: indexPath) as! EventListCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let context = self.fetchedResultsController.managedObjectContext
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject)
                
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                print("Unresolved error \(error)")
                //abort()
            }
        }
    }

    func configureCell(cell: EventListCell, atIndexPath indexPath: NSIndexPath) {
        if let eventList :EventList = self.fetchedResultsController.objectAtIndexPath(indexPath) as? EventList {

            if let title = eventList.title {
                cell.titleLabel!.text = title
            } else {
                cell.titleLabel!.text = ""
            }
            
            if let subtitle = eventList.subtitle {
                cell.subtitleTextView!.text = subtitle
            } else {
                cell.subtitleTextView!.text = ""
            }
            
            if let eventCount = eventList.event?.count {
                if let events = eventList.event {
                    cell.eventCountLabel!.text = "\(events.count) marks"
                    switch eventCount {
                    case 0:
                        cell.detailTextLabel?.textColor = UIColor.lightGrayColor()
                    default:
                        cell.detailTextLabel?.textColor = UIColor.darkGrayColor()
                    }
                } else {
                    cell.eventCountLabel!.text = ""
                }
            } else {
                cell.eventCountLabel!.text = ""
            }

            if let lastUpdated = eventList.dateUpdated {
                cell.lastEditedLabel!.text = "Last updated: \(dateFormatter.stringFromDate(lastUpdated))"
            } else {
                cell.lastEditedLabel!.text = ""
            }
        
        }
    }

    // MARK: - Fetched results controller

    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("EventList", inManagedObjectContext: dataModel.managedObjectContext)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "dateCreated", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: dataModel.managedObjectContext, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             //print("Unresolved error \(error), \(error.userInfo)")
             abort()
        }
        
        return _fetchedResultsController!
    }    
    var _fetchedResultsController: NSFetchedResultsController? = nil

    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
            case .Insert:
                self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            default:
                return
        }
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
            case .Insert:
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Update:
                self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)! as! EventListCell, atIndexPath: indexPath!)
            case .Move:
                tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }

    /*
     // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
     
     func controllerDidChangeContent(controller: NSFetchedResultsController) {
         // In the simplest, most efficient, case, reload the table view.
         self.tableView.reloadData()
     }
     */

    func insertNewObject(sender: AnyObject) {
        let context = self.fetchedResultsController.managedObjectContext
        let entity = self.fetchedResultsController.fetchRequest.entity!
        if let newEventList :EventList = NSEntityDescription.insertNewObjectForEntityForName(entity.name!, inManagedObjectContext: context) as? EventList {
            
            // If appropriate, configure the new managed object.
            newEventList.dateCreated = NSDate()
                        
            // Save the context.
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }

            //present event list editor at this point
            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            if let controller :EditEventListViewController = storyboard.instantiateViewControllerWithIdentifier("EditEventListViewController") as? EditEventListViewController {
                controller.eventList = newEventList
                controller.delegate = self
                presentViewController(controller, animated: true, completion: nil)
            }

        }
    }
    
    func dismissPresentedController() {
        dismissViewControllerAnimated(true, completion: {
            self.tableView.reloadData()
        })
    }
    
    func removeChildController() {
        
    }

    class func blankDetailViewController() -> UIViewController {
//        //only replce the detail view if we're displaying master/detail split
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        let splitViewController = appDelegate.window!.rootViewController as! UISplitViewController
//        if splitViewController.viewControllers.count>1 {
            let storyboard = UIStoryboard(name:"Main", bundle:nil)
            let controller = storyboard.instantiateViewControllerWithIdentifier("HomeViewController")
        return controller
//            let navigationController = splitViewController.viewControllers.last as! UINavigationController
//            navigationController.setViewControllers([controller], animated: true)
//        }

    }
    
}

