//
//  EditEventListViewController.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit
import CoreData

protocol DismissControllerDelegate {
    func dismissPresentedController()
    func removeChildController()
}

class EditEventListViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var titleTextview: UITextView!
    @IBOutlet weak var subtitleTextview: UITextView!
    @IBOutlet weak var informationLabel: UILabel!

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var importButton: UIButton!

    var delegate :DismissControllerDelegate? = nil

    let dataModel = DataModel.sharedInstance

    var eventList: EventList? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if let saveButton = self.saveButton {
            saveButton.enabled = false
        }
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureView() {
        if let eventList = self.eventList {
            let backgroundColor = UIColor(red: 255.0/255.0, green: 252/255.0, blue: 210/255.0, alpha: 1.0)
            if let titleTextview = self.titleTextview {
                titleTextview.text = eventList.title
                titleTextview.backgroundColor = backgroundColor
            }
            if let subtitleTextview = self.subtitleTextview {
                subtitleTextview.text = eventList.subtitle
                subtitleTextview.backgroundColor = backgroundColor
            }
            if let titleTextview = self.titleTextview {
                titleTextview.becomeFirstResponder()
                if eventList.dateCreated == eventList.dateUpdated {
                    //hasn't been edited by user yet, select all text
                    titleTextview.selectedTextRange = titleTextview.textRangeFromPosition(titleTextview.beginningOfDocument, toPosition: titleTextview.endOfDocument)
                }
                
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textViewDidChange(textView: UITextView) {
        saveButton.enabled = true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        switch textView {
        case titleTextview:
            subtitleTextview.becomeFirstResponder()
        case subtitleTextview:
            didTapSaveButton(self)
        default:
            break
        }
    }

    @IBAction func didTapSaveButton(sender: AnyObject) {
        if let eventList = self.eventList {
            eventList.title = titleTextview.text
            eventList.subtitle = subtitleTextview.text
            eventList.dateUpdated = NSDate()
            
            do {
                try dataModel.managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
            
        }
        
        //now, dismiss
        if let delegate = self.delegate {
            delegate.dismissPresentedController()
        }
    }

    @IBAction func didTapCancelButton(sender: AnyObject) {
        titleTextview.resignFirstResponder()
        subtitleTextview.resignFirstResponder()
        dataModel.managedObjectContext.reset()
        if let delegate = self.delegate {
            delegate.dismissPresentedController()
        }
    }

    @IBAction func didTapImportButton(sender: AnyObject) {
        
            if let clipboardString = UIPasteboard.generalPasteboard().string {
                
                eventList = EventList.importMarkdown(clipboardString, context: dataModel.managedObjectContext)
                
//                let eventListStruct :EventListStruct = clipboardString.parseMarkdownForEventList()
//                
//                if let title = eventListStruct.title {
//                    eventList?.title = title
//                }
//                
//                if let subtitle = eventListStruct.subtitle {
//                    eventList?.subtitle = subtitle
//                }
//                
//                for object in eventListStruct.events {
//                    if let newEvent :Event = NSEntityDescription.insertNewObjectForEntityForName("Event", inManagedObjectContext: context) as? Event {
//                        let dictionary :Dictionary<String, AnyObject> = object
//
//                        newEvent.dateCreated = NSDate()
//                        newEvent.dateUpdated = newEvent.dateCreated
//                        if let title = dictionary["title"] as? String {
//                            newEvent.title = title
//                        }
//                        if let subtitle = dictionary["subtitle"] as? String  {
//                            newEvent.subtitle = subtitle
//                        }
//                        newEvent.timeStamp = -1
//                        newEvent.eventList = eventList
//                    }
//                }
                
                if let label = informationLabel {
                    if let count = eventList?.event?.count {
                        label.text = "\(count) events imported"
                    }
                }

//                eventList?.dateUpdated = NSDate()
                
                configureView()
                saveButton.enabled = true
    
                exportMarkdown()
                
            }
    }

    @IBAction func didTapExportButton(sender: AnyObject) {
        
//        if let context = self.managedObjectContext {
//            
//            if let clipboardString = UIPasteboard.generalPasteboard().string {
//                
//                eventList = EventList.importMarkdown(clipboardString, context: context)
//                
//                //                let eventListStruct :EventListStruct = clipboardString.parseMarkdownForEventList()
//                //
//                //                if let title = eventListStruct.title {
//                //                    eventList?.title = title
//                //                }
//                //
//                //                if let subtitle = eventListStruct.subtitle {
//                //                    eventList?.subtitle = subtitle
//                //                }
//                //
//                //                for object in eventListStruct.events {
//                //                    if let newEvent :Event = NSEntityDescription.insertNewObjectForEntityForName("Event", inManagedObjectContext: context) as? Event {
//                //                        let dictionary :Dictionary<String, AnyObject> = object
//                //
//                //                        newEvent.dateCreated = NSDate()
//                //                        newEvent.dateUpdated = newEvent.dateCreated
//                //                        if let title = dictionary["title"] as? String {
//                //                            newEvent.title = title
//                //                        }
//                //                        if let subtitle = dictionary["subtitle"] as? String  {
//                //                            newEvent.subtitle = subtitle
//                //                        }
//                //                        newEvent.timeStamp = -1
//                //                        newEvent.eventList = eventList
//                //                    }
//                //                }
//                
//                if let label = informationLabel {
//                    if let count = eventList?.event?.count {
//                        label.text = "\(count) events imported"
//                    }
//                }
//                
//                //                eventList?.dateUpdated = NSDate()
//                
//                configureView()
//                saveButton.enabled = true
//                
//            }
//        }
    }

    
    func exportMarkdown() {
        if let eventList = self.eventList {
            let markdown = eventList.markdown()
            print("markdown:\n\(markdown)\n")
        }
    }
    
}
