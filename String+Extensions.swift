//
//  String+Extensions.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import Foundation

struct EventListStruct {
    var title :String? = ""
    var subtitle :String? = ""
    var events :[Dictionary<String, AnyObject>]
}

extension String {
    
    //parse for title: if line begins with '# '
    func isTitleLine(line :String) -> Bool {
        let match = "# "
        if line.characters.count < match.characters.count {
            return false
        }
        let index = line.startIndex.advancedBy(match.characters.count)
        let substring = line.substringToIndex(index)
        return substring == match
    }
    func titleFromLine(line :String) -> String {
        let match = "# "
        if line.characters.count < match.characters.count {
            return ""
        }
        let index = line.startIndex.advancedBy(match.characters.count)
        let substring = line.substringFromIndex(index)
        return substring
    }
    
    //parse for title: if line begins with '# '
    func isSubtitleLine(line :String) -> Bool {
        let match = "## "
        if line.characters.count < match.characters.count {
            return false
        }
        let index = line.startIndex.advancedBy(match.characters.count)
        let substring = line.substringToIndex(index)
        return substring == match
    }
    func subtitleFromLine(line :String) -> String {
        let match = "## "
        if line.characters.count < match.characters.count {
            return ""
        }
        let index = line.startIndex.advancedBy(match.characters.count)
        let substring = line.substringFromIndex(index)
        return substring
    }
    
    //parse for event title: if line begins with '- '
    func isEndOfEventLine(line :String) -> Bool {
        let match = ""
        return line == match
    }
    
    func parseEventLines(lines :[String]) -> [String :String] {
        var event :[String :String] = [:]
        var subtitle :[String] = []
        for (_,line) in lines.enumerate() {
            if isEndOfEventLine(line) == false {
                //if title already set, break out
                if let _ = event["title"] {
                    subtitle.append(line)
                } else {
                    event["title"] = line
                }
            } else {
                break
            }
        }
        if subtitle.count > 0 {
            event["subtitle"] = subtitle.joinWithSeparator("\n")
        }
        return event
    }
    
    func parseMarkdownForEventList() -> EventListStruct {
        
        let lines = self.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
        
        var eventList :EventListStruct = EventListStruct(title: "", subtitle: "", events: [])
        
        var lineIndex = 0
        while lineIndex < lines.count {
            let line = lines[lineIndex]

            if eventList.title == "" && isTitleLine(line) {
                //parse for title: if line begins with '# '
                eventList.title = titleFromLine(line)
                lineIndex += 1

            } else if eventList.subtitle == "" && isSubtitleLine(line) {
                //parse for subtitle: if line begins with '## '
                eventList.subtitle = subtitleFromLine(line)
                lineIndex += 1

            } else if isEndOfEventLine(line) == false {
                //parse for events: new event if line begins with '- '
                let remainingLines = Array(lines[lineIndex...lines.count-1]) as [String]
                let event = parseEventLines(remainingLines)
                if event.count > 0 {
                    eventList.events.append(event)

                    if let _ = event["title"] {
                        lineIndex += 1
                    }
                    if let _ = event["subtitle"] {
                        if let count = event["subtitle"]?.componentsSeparatedByString("\n").count {
                            lineIndex += count
                        }
                    }
                    lineIndex += 1
                }

            } else {
                lineIndex += 1
            }
        }
        
        return eventList
    }
    
}

extension Dictionary {
    /// Return a new dictionary with values from `self` and `other`.  For duplicate keys, self wins.
    func combinedWith(var other: Dictionary<Key,Value>) -> Dictionary<Key,Value> {
        for (key, value) in self {
            other[key] = value
        }
        return other
    }
}
