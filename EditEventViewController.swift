//
//  EditEventViewController.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit
import CoreData

class EditEventViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var titleTextview: UITextView!
    @IBOutlet weak var subtitleTextview: UITextView!

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var delegate :DismissControllerDelegate? = nil

    let dataModel = DataModel.sharedInstance

    var isDirty :Bool = false

    var event: Event? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        isDirty = false
        configureView()
        self.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
        self.navigationItem.leftItemsSupplementBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureView() {
        if let event = self.event {
            let backgroundColor = UIColor(red: 255.0/255.0, green: 252/255.0, blue: 210/255.0, alpha: 1.0)
            if let titleTextview = self.titleTextview {
                titleTextview.text = event.title
                titleTextview.backgroundColor = backgroundColor
                titleTextview.textColor = UIColor.darkTextColor()
            }
            if let subtitleTextview = self.subtitleTextview {
                subtitleTextview.text = event.subtitle
                subtitleTextview.backgroundColor = backgroundColor
                subtitleTextview.textColor = UIColor.darkTextColor()
            }
            if let titleTextview = self.titleTextview {
                if event.title == nil {
                    titleTextview.becomeFirstResponder()
                }
//                if event.dateCreated == event.dateUpdated {
//                    //hasn't been edited by user yet, select all text
//                    titleTextview.selectedTextRange = titleTextview.textRangeFromPosition(titleTextview.beginningOfDocument, toPosition: titleTextview.endOfDocument)
//                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textViewDidChange(textView: UITextView) {
        isDirty = true
        //saveButton.enabled = isDirty
    }

    @IBAction func didTapCancelButton(sender: AnyObject) {
        titleTextview.resignFirstResponder()
        subtitleTextview.resignFirstResponder()
        dataModel.managedObjectContext.reset()
        if let delegate = self.delegate {
            delegate.dismissPresentedController()
        }
    }

    @IBAction func didTapSaveButton(sender: AnyObject) {
        //if isDirty {
            
            if let event = self.event {
                event.title = titleTextview.text
                event.subtitle = subtitleTextview.text
                event.dateUpdated = NSDate()
                
                do {
                    try dataModel.managedObjectContext.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    //print("Unresolved error \(error), \(error.userInfo)")
                    abort()
                }
                
                //now, dismiss
                if let delegate = self.delegate {
                    delegate.dismissPresentedController()
                }
            }
        //}
    }
    
}
