//
//  EventListViewController.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import Foundation
import UIKit
import CoreData

enum RunningState {
    case Stopped
    case Running
    case Paused
}

class EventCell: UITableViewCell {
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var textPlaceholderImageView: UIImageView!
    override func awakeFromNib() {
        setup()
    }
    override func prepareForReuse() {
        setup()
    }
    func setup(){
        if let titleTextView = self.titleTextView {
            titleTextView.textContainer.lineFragmentPadding = 0
            titleTextView.textContainerInset = UIEdgeInsetsZero
        }

    }
}

class EventListViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, DismissControllerDelegate {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var addEventButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var editEventListButton: UIButton!
    
    let dataModel = DataModel.sharedInstance

    var timer :NSTimer = NSTimer()
    var runningState :RunningState = .Stopped
    var timeStamp :NSTimeInterval = -1
    var selectedEvent :Event? = nil
    var eventViewController :EventViewController? = nil
    
    var eventList: EventList? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let _ = self.eventList {
            if let _ = tableview {
                
                let longpress = UILongPressGestureRecognizer(target: self, action: "longPressGestureRecognized:")
                longpress.minimumPressDuration = 0.6
                tableview.addGestureRecognizer(longpress)

                tableview.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let addButton = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: "shareEventList:")
        self.navigationItem.rightBarButtonItem = addButton

        self.configureView()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        removeChildController()
        
//        coordinator.animateAlongsideTransition({ (UIViewControllerTransitionCoordinatorContext) -> Void in
//            }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
//        })
        
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapAddButton(sender: AnyObject) {
        insertNewObject(self)
    }

    @IBAction func didTapStartButton(sender: AnyObject) {
        switch runningState {
        case .Stopped:
            startRunning()
        case .Running:
            //insertNewObject(self)
            pauseRunning()
        case .Paused:
            unpauseRunning()
        }
    }
    
    @IBAction func didTapEditEventListButton(sender: AnyObject) {
    }

    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editEventList" {
            if let controller = segue.destinationViewController as? EditEventListViewController {
                controller.eventList = self.eventList
                controller.delegate = self
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("EventCell", forIndexPath: indexPath) as! EventCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let context = self.fetchedResultsController.managedObjectContext
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject)
            
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
        }
    }
    
    func configureCell(cell: EventCell, atIndexPath indexPath: NSIndexPath) {
        if let event :Event = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Event {
            if let title = event.title {
                cell.titleTextView!.text = title
                cell.titleTextView!.hidden = false
                cell.textPlaceholderImageView!.hidden = true
            } else {
                cell.titleTextView!.hidden = true
                cell.textPlaceholderImageView!.hidden = false
            }
            
            if let timeStamp = event.timeStamp {
                if timeStamp != -1 {
                    cell.timeStampLabel!.text = "\(timeStampString(NSTimeInterval(timeStamp)))"
                    cell.titleTextView!.alpha = 1.0
                    cell.textPlaceholderImageView!.alpha = 1.0
                } else {
                    configureCellWithoutTimestamp(cell)
                }
            } else {
                configureCellWithoutTimestamp(cell)
            }

            if let gestures = cell.gestureRecognizers {
                for (_, gesture) in gestures.enumerate() {
                    cell.removeGestureRecognizer(gesture)
                }
            }
        }
    }
    
    func configureCellWithoutTimestamp(cell: EventCell) {
        cell.timeStampLabel!.text = "--:--"
        cell.titleTextView!.alpha = 0.4
        cell.textPlaceholderImageView!.alpha = 0.4
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableview.deselectRowAtIndexPath(indexPath, animated: true)
        
        callActionForIndexPath(indexPath)
    }
    
    @IBAction func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        if let longPress = gestureRecognizer as? UILongPressGestureRecognizer {
            let state = longPress.state
            if state == .Began {

                let keyWindow = UIApplication.sharedApplication().keyWindow
                let initialPoint = longPress.locationInView(keyWindow)

                let locationInView = longPress.locationInView(self.tableview)
                if let indexPath = tableview.indexPathForRowAtPoint(locationInView) {
                    previewActionForIndexPath(indexPath, initialPoint: initialPoint)
                }

            }
        }
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("Event", inManagedObjectContext: dataModel.managedObjectContext)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: "timeStamp", ascending: true),
            NSSortDescriptor(key: "dateCreated", ascending: true)
        ]

        if let eventList = self.eventList {
            let predicate = NSPredicate(format: "eventList = %@", eventList)
            fetchRequest.predicate = predicate
        }
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: dataModel.managedObjectContext, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableview.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableview.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            tableview.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableview.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            tableview.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            self.configureCell(tableview.cellForRowAtIndexPath(indexPath!)! as! EventCell, atIndexPath: indexPath!)
        case .Move:
            tableview.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            tableview.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableview.endUpdates()
    }
    
    func startRunning() {
        runningState = .Running
        timeStamp = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "update", userInfo: nil, repeats: true)
        updateStartButton()
    }

    func pauseRunning() {
        runningState = .Paused
        timer.invalidate()
        update()
        updateStartButton()
    }
    
    func unpauseRunning() {
        runningState = .Running
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "update", userInfo: nil, repeats: true)
        updateStartButton()
    }
    
    func update() {
        var title = "Start"
            switch runningState {
            case .Stopped:
                title = "Start"
            case .Running:
                timeStamp += 1.0
                title = "\(timeStampString(timeStamp))"
            case .Paused:
                title = "\(timeStampString(timeStamp))"
            }

        //startButton.titleLabel?.layer.removeAllAnimations()
        //UIView.setAnimationsEnabled(false)
        UIView.performWithoutAnimation({ () -> Void in
            self.startButton.setTitle(title, forState: .Normal)
            self.startButton.setTitle(title, forState: .Highlighted)
            self.startButton.setTitle(title, forState: .Disabled)
            self.startButton.setTitle(title, forState: .Selected)
            self.startButton.layoutIfNeeded()

        })
        //UIView.setAnimationsEnabled(true)

    }

    func insertNewObject(sender: AnyObject) {
        let context = self.fetchedResultsController.managedObjectContext
        let entity = self.fetchedResultsController.fetchRequest.entity!
        if let newEvent :Event = NSEntityDescription.insertNewObjectForEntityForName(entity.name!, inManagedObjectContext: context) as? Event {
            
            // If appropriate, configure the new managed object.
            newEvent.dateCreated = NSDate()
            newEvent.dateUpdated = newEvent.dateCreated
            if let count = eventList?.event?.count {
                newEvent.title = "Mark \(count)"
            }
            if runningState == .Running {
                newEvent.timeStamp = timeStamp
                // #warning if podcast is running, record 10 seconds of audio & attach to new event
                // #warning (to act as content reminder for the current event)
            }
            newEvent.eventList = self.eventList
            
            // Save the context.
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
            
//            //present event editor at this point
//            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//            if let controller :EditEventViewController = storyboard.instantiateViewControllerWithIdentifier("EditEventViewController") as? EditEventViewController {
//                controller.event = newEvent
//                controller.delegate = self
//                controller.managedObjectContext = self.managedObjectContext
//                presentViewController(controller, animated: true, completion: nil)
//            }

            tableview.reloadData()
            
        }
    }
    
    func timeStampString(timeStamp : NSTimeInterval) -> String {
        let dateComponentsFormatter = NSDateComponentsFormatter()
        dateComponentsFormatter.zeroFormattingBehavior = .Pad
        dateComponentsFormatter.allowedUnits = [NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]
        if let timeStampString = dateComponentsFormatter.stringFromTimeInterval(timeStamp) {
            return timeStampString
        } else {
            return "--:--"
        }
    }

    func callActionForIndexPath(indexPath :NSIndexPath){
        switch runningState {
        case .Stopped:
            editEventAtIndexPath(indexPath)
        case .Paused:
            editEventAtIndexPath(indexPath)
        case .Running:
            setTimestampForEventAtIndexPath(indexPath)
        }
    }

    func previewActionForIndexPath(indexPath :NSIndexPath, initialPoint :CGPoint) {
        if let event :Event = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Event {
            
            //present event list editor at this point
            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            if let controller :EventViewController = storyboard.instantiateViewControllerWithIdentifier("EventViewController") as? EventViewController {
                controller.delegate = self
                controller.event = event
                eventViewController = controller
                
                controller.initialPoint = initialPoint
                
                let keyWindow = UIApplication.sharedApplication().keyWindow
                keyWindow?.addSubview(controller.view)
                
//                keyWindow?.leadingAnchor.constraintEqualToAnchor(controller.view.leadingAnchor, constant: 0)
//                keyWindow?.trailingAnchor.constraintEqualToAnchor(controller.view.trailingAnchor, constant: 0)
//                keyWindow?.topAnchor.constraintEqualToAnchor(controller.view.topAnchor, constant: 0)
//                keyWindow?.bottomAnchor.constraintEqualToAnchor(controller.view.bottomAnchor, constant: 0)
                
            }
        }
    }

    func editEventAtIndexPath(indexPath :NSIndexPath) {
        if let event :Event = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Event {
            selectedEvent = event
            //present event list editor at this point
            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            if let controller :EditEventViewController = storyboard.instantiateViewControllerWithIdentifier("EditEventViewController") as? EditEventViewController {
                controller.event = event
                controller.delegate = self

                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let splitViewController = appDelegate.window!.rootViewController as! UISplitViewController
                if splitViewController.viewControllers.count == 1 {
                    //not in split mode
                    self.navigationController?.pushViewController(controller, animated: true)
                } else {
                    splitViewController.showDetailViewController(controller, sender: self)
                }
            }
        }
    }
    
    func setTimestampForEventAtIndexPath(indexPath :NSIndexPath){
        if let event :Event = self.fetchedResultsController.objectAtIndexPath(indexPath) as? Event {
            event.timeStamp = timeStamp
            event.dateUpdated = NSDate()
            
            let context = self.fetchedResultsController.managedObjectContext
            
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //print("Unresolved error \(error), \(error.userInfo)")
                abort()
            }

        }
    }

    func updateStartButton(){
        switch runningState {
        case .Stopped :
            startButton.backgroundColor = UIColor.clearColor()
            startButton.setTitleColor(UIColor.redColor(), forState:.Normal)
            
        case .Running :
            startButton.backgroundColor = UIColor.redColor()
            startButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            
        case .Paused :
            startButton.backgroundColor = UIColor.redColor().colorWithAlphaComponent(0.5)
            startButton.setTitleColor(UIColor.whiteColor(), forState:.Normal)

        }
    }
    
    func shareEventList(sender :AnyObject) {
        if let markdown = eventList?.markdown() {
            let activityVC = UIActivityViewController(activityItems: [markdown], applicationActivities: nil)

            activityVC.completionWithItemsHandler = {
                (string :String?, complete :Bool, objects :[AnyObject]?, error :NSError?) in
                
                if complete {
                    let alert = UIAlertController(title: "Done!", message: nil, preferredStyle: .Alert)
                    self.presentViewController(alert,animated: true, completion: {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
                            self.dismissViewControllerAnimated(true, completion: nil)
                        })
                    })
                    
                } else {
                    if let errorString = error?.localizedDescription {
                        let alert = UIAlertController(title: "Error", message: errorString, preferredStyle: .Alert)
                        self.presentViewController(alert,animated: true, completion: nil)
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(2.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
                            self.dismissViewControllerAnimated(true, completion: nil)
                        })
                    }
                }
                
            }

        self.presentViewController(activityVC, animated: true, completion: nil)
        }
    }

    func dismissPresentedController() {
        dismissViewControllerAnimated(false, completion: {
            self.eventViewController = nil
            self.tableview.reloadData()
        })
    }
    
    func removeChildController() {
        if eventViewController != nil {
            eventViewController?.view.removeFromSuperview()
            eventViewController = nil
        }
    }
    
}
