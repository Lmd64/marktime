//
//  EventList+CoreDataProperties.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension EventList {

    @NSManaged var dateCreated: NSDate?
    @NSManaged var dateUpdated: NSDate?
    @NSManaged var title: String?
    @NSManaged var subtitle: String?
    @NSManaged var archived: NSNumber?
    @NSManaged var event: NSSet?

}
