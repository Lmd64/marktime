//
//  Event.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import Foundation
import CoreData

class Event: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    func timeStampString() -> String {
        if let timeStamp = self.timeStamp {
            
            let dateComponentsFormatter = NSDateComponentsFormatter()
            dateComponentsFormatter.zeroFormattingBehavior = .Pad
            dateComponentsFormatter.allowedUnits = [NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]
            
            if timeStamp.doubleValue > 0.0 {
                if let timeStampString = dateComponentsFormatter.stringFromTimeInterval(timeStamp.doubleValue) {
                    return timeStampString
                }
            }
        }
        
        return "--:--"
    }

    func markdown() -> String {
        var markdown :String = ""
        
        markdown = markdown + "\(self.timeStampString()) "
        
        if let title = self.title {
            if title.characters.count > 0 {
                markdown = markdown + "\(title)\n"
            }
        }
        if let subtitle = self.subtitle {
            if subtitle.characters.count > 0 {
                markdown = markdown + "\(subtitle)\n"
            }
        }

        markdown = markdown.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        return markdown
    
    }

}
