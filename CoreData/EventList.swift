//
//  EventList.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import Foundation
import CoreData

class EventList: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

    class func importMarkdown(string :String, context :NSManagedObjectContext) -> EventList? {
        
        let eventListStruct = string.parseMarkdownForEventList()
        
        if let eventList :EventList = NSEntityDescription.insertNewObjectForEntityForName("EventList", inManagedObjectContext: context) as? EventList {
            
            if let title = eventListStruct.title {
                eventList.title = title
            }
            
            if let subtitle = eventListStruct.subtitle {
                eventList.subtitle = subtitle
            }
            
            for object in eventListStruct.events {
                if let newEvent :Event = NSEntityDescription.insertNewObjectForEntityForName("Event", inManagedObjectContext: context) as? Event {
                    let dictionary :Dictionary<String, AnyObject> = object
                    
                    newEvent.dateCreated = NSDate()
                    newEvent.dateUpdated = newEvent.dateCreated
                    if let title = dictionary["title"] as? String {
                        newEvent.title = title
                    }
                    if let subtitle = dictionary["subtitle"] as? String  {
                        newEvent.subtitle = subtitle
                    }
                    newEvent.timeStamp = -1
                    newEvent.eventList = eventList
                }
            }
            
            eventList.dateUpdated = NSDate()
            
            return eventList
            
        } else {
            return nil
        }
        
    }
    
    func markdown() -> String {
        var markdown :String = ""
        var didAddTitle = false

        if let title = self.title {
            if title.characters.count > 0 {
                markdown = markdown + "# \(title)\n"
                didAddTitle = true
            }
        }
        if let subtitle = self.subtitle {
            if subtitle.characters.count > 0 {
                markdown = markdown + "## \(subtitle)\n"
                didAddTitle = true
            }
        }
        if didAddTitle {
            markdown = markdown + "\n"
        }
        
        let sortDescriptorTimestamp = NSSortDescriptor(key: "timeStamp", ascending: true)
        let sortDescriptorDateCreated = NSSortDescriptor(key: "dateCreated", ascending: true)
        let sortDescriptorTitle = NSSortDescriptor(key: "title", ascending: true)
        if let sortedEvents = self.event?.sortedArrayUsingDescriptors([sortDescriptorTimestamp, sortDescriptorDateCreated,sortDescriptorTitle]) {
            for object in sortedEvents {
                if let event = object as? Event {
                    
                    let eventMarkdown = event.markdown()
                    
                    if eventMarkdown != "" {
                        markdown = markdown + "\(eventMarkdown)\n\n"
                    }
                }
            }
        }

        markdown = markdown.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())

        return markdown
        
    }
    
}
