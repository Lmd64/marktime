//
//  EventViewController.swift
//  MarkTime
//
//  Created by Liam Dunne on 28/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit

class EventViewController :UIViewController, UIGestureRecognizerDelegate  {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var subtitleTextView: UITextView!
    @IBOutlet weak var contentCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentCenterYConstraint: NSLayoutConstraint!

    var delegate :DismissControllerDelegate? = nil
    
    let dataModel = DataModel.sharedInstance
    let backgroundColorAlpha :CGFloat = 0.75
    let cornerRadius :CGFloat = 8.0
    
    var initialPoint :CGPoint = UIApplication.sharedApplication().keyWindow!.center
    
    var event: Event? {
        didSet {
            // Update the view.
            //self.configureView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
        configureView()
        if let contentView = self.contentView {
            contentView.transform = CGAffineTransformMakeScale(0.1, 0.1)
            contentView.center = initialPoint
            UIView.animateWithDuration(0.32, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.3, options: .BeginFromCurrentState, animations: {
                contentView.transform = CGAffineTransformIdentity
                contentView.center = self.view!.center
                }, completion: {
                    (complete :Bool) in
            })
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        view.setNeedsDisplay()
        view.setNeedsLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func log() {
        print("view                  = \(self.view!)")
        print("view.alpha              = \(self.view!.alpha)")
        print("view.hidden             = \(self.view!.hidden)")
        print("contentView           = \(self.contentView!)")
        print("contentView.alpha       = \(self.contentView!.alpha)")
        print("contentView.hidden      = \(self.contentView!.hidden)")
        print("cancelButton          = \(self.cancelButton!)")
        print("cancelButton.alpha      = \(self.cancelButton!.alpha)")
        print("cancelButton.hidden     = \(self.cancelButton!.hidden)")
        print("timestampLabel        = \(self.timestampLabel!)")
        print("timestampLabel.alpha    = \(self.timestampLabel!.alpha)")
        print("timestampLabel.hidden   = \(self.timestampLabel!.hidden)")
        print("titleTextView         = \(self.titleTextView!)")
        print("titleTextView.alpha     = \(self.titleTextView!.alpha)")
        print("titleTextView.hidden    = \(self.titleTextView!.hidden)")
        print("subtitleTextView      = \(self.subtitleTextView!)")
        print("subtitleTextView.alpha  = \(self.subtitleTextView!.alpha)")
        print("subtitleTextView.hidden = \(self.subtitleTextView!.hidden)")
        //print("event            = \(self.event!)")
    }
    
    func configureView() {

        view.backgroundColor = view.backgroundColor?.colorWithAlphaComponent(0.0)
        self.contentView!.alpha = 0.0

        if let contentView = self.contentView {
            
            UIView.animateWithDuration(0.32, animations: {
                self.view.backgroundColor = self.view.backgroundColor?.colorWithAlphaComponent(self.backgroundColorAlpha)
                contentView.alpha = 1.0
                }, completion: {
                    (complete :Bool) in
                    self.setContentShadow()
            })
        }
        
        if let event = self.event {
            if let timestampLabel = self.timestampLabel {
                timestampLabel.text = event.timeStampString()
                timestampLabel.textColor = UIColor.darkGrayColor()
            }
            if let titleTextview = self.titleTextView {
                titleTextview.text = event.title
                titleTextview.textColor = UIColor.darkGrayColor()
            }
            if let subtitleTextview = self.subtitleTextView {
                subtitleTextview.text = event.subtitle
                subtitleTextview.textColor = UIColor.darkGrayColor()
            }
            view.setNeedsDisplay()
            view.setNeedsLayout()
        }
        

    }
    
    @IBAction func didTapCancelButton(sender: AnyObject) {
        if let contentView = self.contentView {
            UIView.animateWithDuration(0.64, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.3, options: .BeginFromCurrentState, animations: {
                contentView.transform = CGAffineTransformMakeScale(0.1, 0.1)
                contentView.center = self.initialPoint
                contentView.alpha = 0.0
                self.view.alpha = 0.0
                }, completion: {
                    (complete :Bool) in
                    self.dismissViewController()
            })
        }
    }
    
    @IBAction func panGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            let state = pan.state
            switch state {
            case .Began:
                break
            case .Changed:
                let offset = pan.translationInView(view)
                setContentOffset(offset)
                setContentShadow()
                let distance = hypoteneuse(offset.x, y: offset.y)
                let viewSize = hypoteneuse(view.frame.size.width, y: view.frame.size.height) / 2.0
                let normalizedDistance = CGFloat(1.0 - distance / viewSize)
                self.view.backgroundColor = self.view.backgroundColor?.colorWithAlphaComponent(backgroundColorAlpha * normalizedDistance)

                break
            case .Ended:
                let offset = pan.translationInView(view)
                let distance = sqrt((offset.x * offset.x) + (offset.y * offset.y))
                let velocity = pan.velocityInView(view)
                let speed = hypoteneuse(velocity.x, y: velocity.y)
                print("distance = \(distance)")
                print("speed    = \(speed)")
                if distance < 32.0 && speed < 50.0 {
                    //reset
                    let velocity = CGPointZero
                    UIView.animateWithDuration(0.5, animations: {
                        self.setContentOffset(velocity)
                        //self.contentView.alpha = 1.0
                        self.view.backgroundColor = self.view.backgroundColor?.colorWithAlphaComponent(self.backgroundColorAlpha)
                        }, completion: {
                            (complete :Bool) in
                    })
                } else {
                    //dismiss
                    UIView.animateWithDuration(0.5, animations: {
                        self.setContentOffset(velocity)
                        self.view.backgroundColor = self.view.backgroundColor?.colorWithAlphaComponent(0.0)
                        self.contentView!.transform = CGAffineTransformMakeScale(0.1, 0.1)
                        self.contentView!.alpha = 0.0
                        }, completion: {
                            (complete :Bool) in
                            self.dismissViewController()
                    })
                }

                
            case .Cancelled:
                break
            case .Failed:
                break
            default:
                break
            }
        }
    }

    func hypoteneuse(x :CGFloat, y :CGFloat) -> Double {
        return sqrt(Double(x * x) + Double(y * y))
    }
    
    func setContentOffset(offset :CGPoint) {
        contentCenterXConstraint.constant = offset.x
        contentCenterYConstraint.constant = offset.y
        view.layoutIfNeeded()
    }
    
    func dismissViewController() {
        if let delegate = self.delegate {
            //delegate.dismissPresentedController()
            delegate.removeChildController()
        }
    }
    
    func setContentShadow(){
        if let contentView = self.contentView {
            let layer = contentView.layer
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = false
            layer.shadowColor = UIColor.blackColor().CGColor
            layer.shadowOffset = CGSizeMake(0, 2)
            layer.shadowOpacity = 0.8
            layer.shadowPath = CGPathCreateWithRoundedRect(contentView.bounds, cornerRadius, cornerRadius, nil)
        }
    }
    
}