//
//  UIColor+Extension.swift
//  MarkTime
//
//  Created by Liam Dunne on 28/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit

extension UIColor {

    class func defaultColor() -> UIColor {
        return UIColor(red: 117, green: 156, blue: 162, alpha: 1.0)
    }

}
