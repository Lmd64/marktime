//
//  AppDelegate.swift
//  MarkTime
//
//  Created by Liam Dunne on 26/01/2016.
//  Copyright © 2016 Lmd64. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    let dataModel = DataModel.sharedInstance

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        let splitViewController = self.window!.rootViewController as! UISplitViewController

        let navigationController = splitViewController.viewControllers.first as! UINavigationController
        //let navigationController = splitViewController.viewControllers.last as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
        splitViewController.delegate = self
        splitViewController.preferredDisplayMode = .AllVisible

        //navigationController.navigationBar.barTintColor
        navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.defaultColor()]

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        dataModel.saveContext()
    }

    // MARK: - Split view
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
        
        let primaryAsNavController = primaryViewController as? UINavigationController
        
        if let editEventViewController = secondaryViewController as? EditEventViewController {
            primaryAsNavController?.pushViewController(editEventViewController, animated: false)
        }
        
        return true
    }

    func splitViewController(splitViewController: UISplitViewController, separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController) -> UIViewController? {

        if let masterNavigationController = primaryViewController as? UINavigationController {
            
            if let editEventViewController = masterNavigationController.topViewController as? EditEventViewController {
                //pull out the edit view controller, and pop from the master stack
                masterNavigationController.popViewControllerAnimated(false)
                return editEventViewController
            } else {
                //just return a blank detail
                let controller = EventListsViewController.blankDetailViewController()
                return controller
            }
        } else {
            //just return a blank detail
            let controller = EventListsViewController.blankDetailViewController()
            return controller
        }
        
    }
    
}

